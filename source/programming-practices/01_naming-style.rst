===========================================================================
命名风格
===========================================================================
#. 【强制】代码中的命名只能使用 ``ASCII 字母`` 和 ``数字`` ，不允许使用下划线（ ``_`` ）或美元符号（ ``$`` ）等字符。

   *反例：*  ``_name`` 、 ``__name`` 、 ``$Object`` 、 ``name_`` 、 ``name$`` 、 ``Object$`` 、 ``s_name`` 、 ``user_name`` 。

#. 【强制】代码中的命名严禁使用拼音与英文混合的方式，更不允许直接使用中文的方式。 说明：正确的英文拼写和语法可以让阅读者易于理解，避免歧义。注意，即使纯拼音命名方式 也要避免采用。也要避免采用。

   *正例：* google、youtube 等国际通用的名称，可视为英文。

   *反例：* getYongHuMing（用户名）、setNianLin（年龄）。

#. 【强制】包名全部小写，连续的单词只是简单地连接起来，不使用下划线。包名统一使用 **单数形式** ，但是类名如果有复数含义，类名可以使用复数形式。

   *正例：* 应用工具类包名为 com.xxx.xxx.util、类名为 MessageUtils（此规则参考 spring 的框架结构）

#. 【强制】类名使用 UpperCamelCase 风格，必须遵从驼峰形式，但以下情形例外：DO、BO、DTO、VO、AO。

   *正例：* MarcoPolo、UserDO、XmlService、TcpUdpDeal、TaPromotion。

   *反例：* macroPolo、UserDo、XMLService、TCPUDPDeal、TAPromotion。

#. 【强制】方法名、参数名、成员变量、局部变量都统一使用 lowerCamelCase 风格，必须遵从驼峰形式。

   *正例：* localValue、getHttpMessage()、inputUserId

#. 【强制】常量命名全部大写，单词间用下划线隔开，力求语义表达完整清楚，不要嫌名字长。

   *正例：* MAX_STOCK_COUNT

#. 【强制】抽象类命名使用 ``Abstract`` 或 ``Base`` 开头;异常类命名使用 ``Exception`` 结尾;测试类命名以它要测试的类的名称开始，以 ``Test`` 或 ``Tests`` 结尾。

#. 【强制】中括号是数组类型的一部分，数组定义如下： ``String[] args`` 。

   *反例：* 使用 ``String args[]`` 的方式来定义。

#. 【强制】POJO 类中的布尔类型的变量，都不要加 ``is`` ，否则部分框架解析会引起序列化错误。

   *反例：* 定义为基本数据类型 ``Boolean isDeleted`` 的属性，它的方法是 isDeleted()，RPC 框架在反向解析的时候，以为对应的属性名称是 deleted，导致属性获取不到，进而抛出异常。

#. 【强制】杜绝完全不规范的缩写，避免望文生义。

   *反例：* AbstractClass 缩写命名成 AbsClass，condition 缩写命名成 condi，此类随意缩写严重降低了代码的可阅读性。

#. 【推荐】如果使用到了设计模式，建议在类名中体现出具体模式。说明：将设计模式体现在名字中，有利于阅读者快速理解架构设计思想。

   *正例：*

   .. code-block:: java

    public class OrderFactory {}
    public class LoginProxy {}
    public class ResouceObserver {}

#. 【推荐】接口类中的方法和属性不要加任何修饰符号（ ``public`` 也不要加），保持代码的简洁性，并加上有效的 Javadoc 注释。尽量不要在接口里定义变量，如果一定要定义变量，肯定是与接口方法相关，并且是整个应用的基础常量。

   *正例：*

   .. code-block:: java

    void f();   // 接口方法签名
    String COMPANY = "xxxx";    // 接口基础常量表示

   *反例：*

   .. code-block:: java

    public abstract void f();   // 接口方法定义

#. 接口和实现类的命名有两套规则：

   #. 【强制】对于 Service 和 DAO 类，基于 SOA 的理念，暴露出来的服务一定是接口，内部的实现类用 Impl 的后缀与接口区别。

      *正例：* CacheServiceImpl 实现 CacheService 接口。

   #. 【推荐】如果是形容能力的接口名称，取对应的形容词做接口名（通常是 ``-able`` 的形式）。

      *正例：* AbstractTranslator 实现 Translatable。

#. 【强制】枚举类名带上 Enum 后缀，枚举成员名称需要全大写，单词间用下划线隔开。说明：枚举其实就是特殊的常量类，且构造方法被默认强制是私有。

   *正例：* 枚举名字 ``DealStatusEnum`` ，成员名称： ``SUCCESS`` 、 ``UNKOWN_REASON`` 。

#. 【推荐】各层命名规约：

   *Service、DAO 层方法命名规约：*

      - 获取单个对象的方法用 ``get`` 做前缀。
      - 获取多个对象的方法用 ``list`` 做前缀。
      - 获取统计值的方法用 ``count`` 做前缀。
      - 插入的方法用 ``save`` （推荐）或 ``insert`` 做前缀。
      - 删除的方法用 ``remove`` （推荐）或 ``delete`` 做前缀。
      - 修改的方法用 ``update`` 做前缀。

   *领域模型命名规约：*

      - 数据对象： ``xxxDO`` ， ``xxx`` 即为数据表名。
      - 数据传输对象： ``xxxDTO`` ， ``xxx`` 为业务领域相关的名称。
      - 展示对象： ``xxxVO`` ， ``xxx`` 一般为网页名称。
      - POJO 是 DO/DTO/BO/VO 的统称，禁止命名成 ``xxxPOJO`` 。
