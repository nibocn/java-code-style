===========================================================================
代码格式
===========================================================================

#. 【强制】大括号的使用约定。如果是大括号内为空，则简洁地写成 {} 即可，不需要换行；如果是非空代码块则：

   #. 左大括号前不换行。

   #. 左大括号后换行。

   #. 右大括号前换行。

   #. 右大括号后还有 else 等代码则不换行 表示终止的右大括号后必须换行。

   示例：

   .. code-block:: java

    return new MyClass() {
        @Override public void method() {
            if (condition()) {
                try {
                    something();
                } catch (ProblemException e) {
                    recover();
                }
            }
        }
    };

   空块，示例：

   .. code-block:: java

    void doNothing() {}

#. 【强制】左小括号和字符之间不出现空格；同样，右小括号和字符之间也不出现空格。

   *正例：* ``if (a == b)``

#. 【强制】if/for/while/switch/do 等保留字与括号之间都必须加空格。

   *正例：*

   .. code-block:: java

    if () {}
    for () {}
    while () {}
    switch () {}
    do () {}

#. 【强制】任何二目、三目运算符的左右两边都需要加一个空格。

   说明：运算符包括赋值运算符=、逻辑运算符&&、加减乘除符号等。

#. 【强制】缩进采用 4 个空格，禁止使用 tab 字符。

   说明：如果使用 tab 缩进，必须设置 1 个 tab 为 4 个空格。IDEA 设置 tab 为 4 个空格时， 请勿勾选 Use tab character；而在 Eclipse 中，必须勾选 insert spaces for tabs。

   *正例：* （涉及 1～5 点）

   .. code-block:: java

    public static void main(String[] args) {
        // 缩进 4 个空格
        String say = "hello";
        // 运算符的左右必须有一个空格
        int flag = 0;
        // 关键词 if 与括号之间必须有一个空格，括号内的 f 与左括号，0 与右括号不需要空格
        if (flag == 0) {
            System.out.println(say);
        }
        // 左大括号前加空格且不换行；左大括号后换行
        if (flag == 1) {
            System.out.println("world");
            // 右大括号前换行，右大括号后有 else，不用换行
        } else {
            System.out.println("ok");
        } // 在右大括号后直接结束，则必须换行
    }

#. 【强制】单行字符数限制不超过 100 个，超出需要换行，换行时遵循如下原则：

   - 第二行相对第一行缩进 4 个空格，从第三行开始，不再继续缩进，参考示例;

   - 运算符与下文一起换行;

   - 方法调用的点符号与下文一起换行;

   - 在多个参数超长时，在逗号后换行;

   - 在括号前不要换行，见反例;

   例外：

   - 不可能满足列限制的行(例如，Javadoc中的一个长URL，或是一个长的JSNI方法参考);

   - package 和 import 语句

   *正例：*

   .. code-block:: java

    StringBuffer sb = new StringBuffer();
    //超过 100 个字符的情况下，换行缩进 4 个空格，并且方法前的点符号一起换行
    sb.append("zi").append("xin")...
        .append("huang")...
        .append("huang")...
        .append("huang");

   *反例：*

   .. code-block:: java

    StringBuffer sb = new StringBuffer();
    //超过 100 个字符的情况下，不要在括号前换行
    sb.append("zi").append("xin")...append
        ("huang");
    //参数很多的方法调用可能超过 100 个字符，不要在逗号前换行
    method(args1, args2, args3, ...
        , argsX);

#. 【强制】方法参数在定义和传入时，多个参数逗号后边必须加空格。

   *正例：* 下例中实参的 ``"a",`` 后边必须要有一个空格。

   .. code-block:: java

    method("a", "b", "c");

#. 【强制】IDE 的 text file encoding 设置为 UTF-8; IDE 中文件的换行符使用 Unix 格式，不要使用 windows 格式。

#. 【不推荐】没有必要增加若干空格来使某一行的字符与上一行对应位置的字符对齐。

   *正例：*

   .. code-block:: java

    int a = 3;
    long b = 4L;
    float c = 5F;
    StringBuffer sb = new StringBuffer();

   说明：增加 sb 这个变量，如果需要对齐，则给 a、b、c 都要增加几个空格，在变量比较多的情况下，是一种累赘的事情。

#. 【推荐】方法体内的执行语句组、变量的定义语句组、不同的业务逻辑之间或者不同的语义之间插入一个空行。相同业务逻辑和语义之间不需要插入空行。

   说明：没有必要插入多个空行进行隔开。

#. 【推荐】枚举常量间用逗号隔开，换行可选。

   说明：没有方法和文档的枚举类可写成数组初始化的格式：

   .. code-block:: java

    private enum Suit {CLUBS, HEARTS, SPADES, DIAMONDS}

#. 【强制】每次只声明一个变量，不要使用组合声明。

   *反例：* ``int a, b;``

#. 【推荐】变量需要时才声明，并尽快进行初始化

   说明：不要在一个代码块的开头把局部变量一次性都声明了(这是 c 语言的做法)，而是在第一次需要使用它时才声明。 局部变量在声明时最好就进行初始化，或者声明后尽快进行初始化。

#. 【强制】注解紧跟在文档块后面，应用于类、方法和构造函数，一个注解独占一行。这些换行不属于自动换行，因此缩进级别不变。

   *正例：*

   .. code-block:: java

    @Override
    @Nullable
    public String getNameIfPresent() { ... }

#. 【参考】单个的注解可以和签名的第一行出现在同一行。示例：

   .. code-block:: java

    @Override public int hashCode() { ... }

#. 【参考】应用于字段的注解紧随文档块出现，应用于字段的多个注解允许与字段出现在同一行。示例：

   .. code-block:: java

    @Partial @Mock DataLoader loader;

   说明：参数和局部变量注解没有特定规则。

#. 块注释与其周围的代码在同一缩进级别。它们可以是 ``/* ... */`` 风格，也可以是 ``// ...`` 风格。对于多行的 ``/* ... */`` 注释，后续行必须从 ``*`` 开始， 并且与前一行的 ``*`` 对齐。以下示例注释都是 OK 的。

   .. code-block:: java

    /*
     * This is          // And so           /* Or you can
     * okay.            // is this.          * even do this. */
     */
