===========================================================================
源文件结构
===========================================================================

package 语句
---------------------------------------------------------------------------
【强制】package 语句不换行，列限制并不适用于 package 语句（即 package 语句写在行里）。

import 语句
---------------------------------------------------------------------------
**import 不要使用通配符**

【强制】不要出现类似这样的 import 语句： ``import java.util.*`` 。

**不要换行**

【强制】import 语句不换行，列限制并不适用于 import 语句（每个 import 语句独立成行）。

**顺序和间距**

【推荐】import 语句可以分为以下几组，按照这个顺序，每组由一个空行分隔：

  #. 所有的静态导入独立成组;
  #. com.xxx imports（仅当这个源文件在 com.xxx 包下）;
  #. 第三方的包。每个顶级包为一组，字典顺序。例如：android，com，junit，org，sun;
  #. java imports;
  #. javax imports;

  *组内不空行，按字典序排列。*

类声明
---------------------------------------------------------------------------
**只有一个顶级类声明**

【强制】每一个顶级类都在一个与它同名的源文件中。

*例外：* package-info.java 该文件中可没有 package-info 类。

**类成员顺序**

【推荐】类的成员顺序对易学性有很大的影响，但这也不存在唯一的通用法则。不同的类对成员的排序可能是不同的。最重要的一点，每个类应该以某种逻辑去排序它的成员，维护者应该要能解释这种排序逻辑。比如，新的方法不能总是习惯性地添加到类的结尾，因为这样就是按时间顺序而非某种逻辑来排序的。

**重载：永不分离**

【强制】当一个类有多个构造函数，或是多个同名方法，这些函数/方法应该按顺序出现在一起，中间不要放进其它函数/方法。
